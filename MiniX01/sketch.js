// runs once, good for settings
function setup() {
  createCanvas(800, 600); 
  background(255, 255, 255);
}
// runs once every 60 secs, can be changed. 
function draw() {
noStroke()

// square 1x1 //
fill(246,226,190,255);
rect(0, 0, 100, 200);
fill(50,48,48,255);
rect(100, 0, 100, 200);
// square 2x1 // 
fill(251,188,46);
rect(200, 0, 200);
fill(53,108,179);
arc(200, 0, 200, 200, 0, HALF_PI);
fill(236,49,69);
arc(400, 200, 200, 200, PI, PI + HALF_PI);
// square 3x1 // 
fill(236,49,69);
rect(400, 0, 200);
fill(246,226,190);
arc(600, 0, 200, 200, HALF_PI, PI);
fill(246,226,190);
arc(400, 200, 200, 200, PI + HALF_PI, TWO_PI);
// square 4x1//
fill(251,188,46);
rect(600, 0, 200, 100);
fill(50,48,48);
rect(600, 100, 200, 100);
// square 1x2 //
fill(236,49,69);
rect(0, 200, 200);
fill(251,188,46);
arc(0, 200, 200, 200, 0, HALF_PI);
fill(246,226,190);
arc(200, 400, 200, 200, PI, PI + HALF_PI);
// square 2x2 // 
fill(53,108,179);
rect(200, 200, 200);
fill(251,188,46);
arc(400, 200, 200, 200, HALF_PI, PI);
fill(251,188,46);
arc(200, 400, 200, 200, PI + HALF_PI, TWO_PI);
// square 3x2
fill(50,48,48);
rect(400, 200, 100, 200);
fill(53,108,179);
rect(500, 200, 100, 200);
// square 4x2 //
fill(246,226,190);
rect(600, 200, 200);
fill(236,49,69);
arc(800, 200, 200, 200, HALF_PI, PI);
fill(251,188,46);
arc(600, 400, 200, 200, PI + HALF_PI, TWO_PI);
// square 1x3 //
fill(251,188,46);
rect(0, 400, 200);
fill(53,108,179);
arc(200, 400, 200, 200, HALF_PI, PI);
fill(50,48,48);
arc(0, 600, 200, 200, PI + HALF_PI, TWO_PI);
// square 2x3 //
fill(236,49,69);
rect(200, 400, 100, 200);
fill(246,226,190);
rect(300, 400, 100, 200);
// sqaure 3x3 //
fill(251,188,46);
rect(400, 400, 200);
fill(236,49,69);
arc(400, 400, 200, 200, 0, HALF_PI);
fill(53,108,179);
arc(600, 600, 200, 200, PI, PI + HALF_PI);
// sqaure 4x3 //
fill(50,48,48);
rect(600, 400, 100, 200);
fill(236,49,69);
rect(700, 400, 100, 200);

}