# Bauhaus graphic

![Bauhaus style graphic](bauhaus.PNG "Bauhaus Graphic")
<br>

Please run the code [here](https://christoffer7430209.gitlab.io/aesthetic-programming/MiniX01)
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->
Please view the full repository [here](https://gitlab.com/christoffer7430209/aesthetic-programming/-/tree/main/MiniX01)
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **My code**
My code creates a graphic display composed of squares and arcs. I've drawn inspiration from the graphic style of Bauhaus and its emphasis on geometry. In my code, I've utilized functions such as 'rect()' to draw rectangles based on specified coordinates, 'arc()' to create parts of a full circle using coordinates and π for calculation, and 'fill()' to add color to my squares and arcs.
<br>

I wanted to explore P5.js's ability to quickly generate art, and I chose Bauhaus because of its geometrical simplicity, which makes it easy to translate into code. Drawing through code is a different approach from traditional methods. Initially, there was a lot of trial and error, but over time, I gained a better understanding of positioning and the coordinate system's layout. As I became more accustomed to the coordinates, interpreting the code and predicting the placement of shapes became easier. It began to make sense, much like learning to read and assembling letters into words.
<br>

Exploring P5.js was a fascinating experience, revealing its potential and distinctiveness compared to conventional digital drawing software like Photoshop or GIMP. For future projects, I plan to focus more on interaction and animation, aspects I didn't explore in this project as I wanted to experiment with something simple."

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
[Reference for "fill" function form P5js.org](https://p5js.org/reference/#/p5/fill) <br>
[Reference for "square" function form P5js.org](https://p5js.org/reference/#/p5/square) <br>
[Reference for "arc" function form P5js.org](https://p5js.org/reference/#/p5/arc) <br>
