// Makes sure the game is not started before you press space
let gameStarted = false
// changes from the start screen to victory screen
let startScreen = true
//makes sure you cant restart the game early by pressing space
let restartGame = false

function setup(){
  createCanvas(windowWidth-4, windowHeight-4)
  rectMode(CENTER)
  noStroke()
  p1 = new Players(LEFT_ARROW, RIGHT_ARROW, DOWN_ARROW, UP_ARROW)
  p2 = new Players(65, 68, 83, 87)
  makeObstacles()
  p1.x += 100
  p2.x -= 100
}

function keyPressed() {
  if (keyCode === 32) { // Space key
    gameStarted = true;
    if(restartGame){
    p1.playerScore = 0
    p2.playerScore = 0
    }
  }
}


let obstacles = []
let increaseInSpeed = 0
let markerPosition = 0

function makeObstacles() {
  while (obstacles.length < 8) {
    obstacles.push(new Obstacles());
  }
}
function roadGraphics(){
// road background
fill(70)
rect(0, 0, 400, 900)

// move the marker down and increases with increaseInSpeed
markerPosition += 2 + increaseInSpeed

// make the row of markers with a forloop to create 15 lines where they are 80 pixels apart  
for (let i = 900; i >= -1200; i -= 80){
  fill(255)
  rect(100, i+markerPosition, 4, 30)
  fill(255);
  rect(-100, i+markerPosition, 4, 30);
}
 
// resets the markerPosition to make a loop animation. Has do be diviable by 80
if(markerPosition >= 640){markerPosition=0}

// yellow lines (for loop er måske lidt overdrevet for 3 liner)
for (let i = -190; i < 380; i += 190) {
  fill(255, 221, 85);
  rect(i, 0, 4, height);
}
// hide the top and bottom
fill(0)
rect(0, 500, 400, 100)
rect(0, -500, 400, 100)
}

// class for Obstacles 
class Obstacles{
  constructor(){
    // starting value of the obstacles 
    this.x = random([-150, -50, 50, 150])
    this.y = -height/2 - floor(random(100, 1000))
    this.w = 50
    this.h = 70
  }
  // makes the obstacles move down, and increase in speed
  move() {
    this.y += 1 + increaseInSpeed
    // resets the obstacles when the go off the screen at the bottom
    if(this.y >= height/2){
      // the .y axis is randomly chosen between 100, 1000 to make them appear at random intevals
      this.y = -height/2-floor(random(100, 1000))
      // i to make sure the players dont cheese the obstacles by staying in the middle of the lane they randomly shift left and right
      this.x = random([floor(random(-140,-160)), floor(random(-30, -60)), floor(random(30, 60)), floor(random(140, 160))])
    }
  }
  // the graphics for the obstacles
  show(){
    fill(0)
    rect(this.x, this.y, this.w, this.h)
  }
  // resets the obstacles placement
  reset(){
    this.y = -height/2-floor(random(100, 1000))
    this.x = random([floor(random(-140,-160)), floor(random(-30, -60)), floor(random(30, 60)), floor(random(140, 160))])
  }
}

// the graphics of the obstacles, the movement and the rect
function obstacleGraphics(){
  for (let i = 0; i < obstacles.length; i++){
    obstacles[i].move()
    obstacles[i].show()
  }
}

// the players Red and Green
// each with different input keys
class Players{
  constructor(leftKey,rightKey,downKey,upKey)
  { 
    // how the player looks
    this.x = 0
    this.y = 200
    this.w = 40
    this.h = 70
    // playerscore and input
    this.playerScore = 0
    this.leftKey = leftKey
    this.rightKey = rightKey
    this.downKey = downKey
    this.upKey = upKey
  } 
  // sets the input by using different parameters
  playerControls(){
    // controls for the players, they can move left, right, up and down. Also makes sure they down leave the road
    if (keyIsDown(this.leftKey)) {
      // checks if the players dont go off the road
      if(this.x > -190){
        // moves the player
        this.x -= 8}
    } 
    if (keyIsDown(this.rightKey)) {
      if(this.x < 190){
        this.x += 8}
    } 
    if (keyIsDown(this.downKey)) {
      if(this.y < 400){
        this.y += 5}
    } 
    if (keyIsDown(this.upKey)) {
      if(this.y > -400){
        this.y -= 5}
  }
  } 

  // this checks collision with the player and obstacles
  // If a player hits and obstacles they opponent gets a point and the gamee restarts
  checkCollisionsObstacles(){
    for (let i = 0; i < obstacles.length; i++) {
      let obs = obstacles[i];
      // Check if the player overlaps with the obstacle
      if (
        this.x + this.w / 2 > obs.x - obs.w / 2 &&
        this.x - this.w / 2 < obs.x + obs.w / 2 &&
        this.y + this.h / 2 > obs.y - obs.h / 2 &&
        this.y - this.h / 2 < obs.y + obs.h / 2
      ) {
        resetGame();
        this.playerScore++
      }
    }
  }
}

// the visuals for the players
function playerGraphics(){
  fill(0, 255, 0);
  rect(p1.x, p1.y, p1.w, p1.h);
  fill(255, 0, 0);
  rect(p2.x, p2.y, p2.w, p2.h);
}

// the start posititon for the players and reset position
function playerResetPosition(){
  p1.x = 100
  p1.y = 200
  p2.x = -100
  p2.y = 200
}

// all the functions together
function draw() {
  translate(width / 2, height / 2);
  background(0)

  roadGraphics()
  playerGraphics()
  
  // makes sure the game dosent start before the players press space
  if(!gameStarted){
    // show startscreen
    fill(255)
    if(startScreen){
    textSize(50)
    textAlign(CENTER)
    text("Press SPACE to start", 0, 0);
    // shows restart screen instead 
    }else{
    textSize(50)
    textAlign(CENTER)
    text("Press SPACE to restart", 0, 0);
    endGame()
    }
  }else{
  // if game start is true the game begins
  if(increaseInSpeed<20){increaseInSpeed += 0.01}

  obstacleGraphics()

  p1.playerControls()
  p2.playerControls()

  p1.checkCollisionsObstacles()
  p2.checkCollisionsObstacles()

  scoreBoard()
  endGame()
  }
}

// resets the obstacles and the player
function resetGame(){
  for (let i = 0; i < obstacles.length; i++){
    obstacles[i].reset()
  }
  increaseInSpeed = 0
  playerResetPosition()
  
}

// shows the players score on each side
function scoreBoard(){
  // gives point to player 1
    for (let i = 0; i < p2.playerScore*40; i += 40){
    fill(0,255,0)
    rect(220,-400+i, 20, 20)
    }
  //gives point to player 2
    for (let i = 0; i < p1.playerScore*40; i += 40){
      fill(255,0,0)
      rect(-220,-400+i, 20, 20)
    }
}

// stops the game and tells who won
// this makes sure the restart screen pops up
function endGame(){
  textSize(50)
  textAlign(CENTER)
  if(p1.playerScore == 3){
    startScreen = false
    gameStarted = false
    restartGame = true
    fill(255,0,0)
    text("RED WINS", 0, -100)
  }
  if(p2.playerScore == 3){
    startScreen = false
    gameStarted = false
    restartGame = true
    fill(0,255,0)
    text("GREEN WINS", 0, -100)
  }
}

//https://p5js.org/reference/#/p5/textAlign
//https://p5js.org/reference/#/p5/text