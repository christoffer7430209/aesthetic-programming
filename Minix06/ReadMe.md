
  

# ROAD GAME

  

  

![GIF](ROAD.gif  "road")

  

<br>

  

  

Please run the code [here](https://christoffer7430209.gitlab.io/aesthetic-programming/Minix06)

  

  

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

  

  

Please view the full repository [here](https://gitlab.com/christoffer7430209/aesthetic-programming/-/tree/main/Minix06)

  

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

  

#### Describe how does/do your game/game objects work? / Describe how you program the objects and their related attributes, and the methods in your game?
My game is a car race between two racers where they need to avoid other cars. Every time the opponent car gets hit the player gets a point. The first car to get 5 points win. The game is split into 3 parts. First part: the background road. Its made of a grey background with yellow and white lines. The white lines is a ´for´ loop that slowly increase in speed. They move faster and faster down the y line. But reset when it hits the bottom. The second part is the players. The green and red square (cars). Each player moves with different key WASD or the arrow keys. The players can move freely on the road but are stopped when they hit the edge. The third part is the obstacles. The obstacles is an ´array´ of objects that spawn randomly over the road and move on screen. They look like the players but are black. The obstacles move the same way the white lines of the road but a bit slower, to look like the players are overlapping cars. The obstacles have 5 positions at the top (the four different car lanes) where they can spawn. The are spawned high over the road (between 100 to 1000 pixel) to look like they are approaching randomly. When an obstacle reaches the bottom, they are put back at the top and randomly placed. This continues until a player hits an obstacle.

#### Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?
The ability to create classes and dedicate objects variables makes programming much easier, and it feels just as essential as an ´for´ loop. When programming we want I to be as easy as possible for both the programmer to code but also for the program to run. By having using OOP its possible to make even more complex programs with even less code.

To break down everything to its most basic components is an interesting way of looking at the world but I need to program anything. When programming this game i always need to know my limits and what that is possible to do with the program and what I’m able to do. By using abstraction, I can make a program that doesn’t look like the world but can give the context clues to what it’s supposed to be. We

#### Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?
My game is a very simple and stylised way of looking at a race between cars. With the cars being squares the only context clue to what is happening is the road. The black squares don’t have to be other cars, it could just as well be rocks or other things blocking the road. The most important part is the players imagination can make it up as they wish. By making my cars simpler and stylised the mimic what emojis used to be. Before they were specialised to show every emotion we have. They were simple symbols put together to try to show the user an emotion like B-) (emoji with sunglasses) or ;_; (crying emoji) it was possible for the user to understand and read the emotion of these symbols even though they were abstraction of real life emotions.

  
  

