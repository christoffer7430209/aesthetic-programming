let videoCapture
let cameraTracker
let isBackgroundBlack = false
let allowDrawing = false
let moneyMade = 0
let timeSinceLastKeyPress = 0
let timeSinceLastMouseClicked = 0
let timeSinceMouseMoved = 0
let consecutiveFalseCount = 0
let showFirstMessage = true

// The setup for the website, matching the window and setting up the videocapture and facetracker.
function setup() {
  createCanvas(windowWidth, windowHeight);
  createVideoCapture();
  setupFaceTracker();
  textFont("Times");
  setInterval(toggleMessage, 400);
}

// Setting up the videofeed that the camera tracker uses to track the face. 
// It uses a 640x480 resolution because the website has a hard time handling higher resolution. 
// At the end i hide the feed because i dont want to show it to the user
function createVideoCapture() {
  videoCapture = createCapture(VIDEO);
  videoCapture.size(640, 480);
  videoCapture.hide();
}

// This is the facetracker that uses the videofeed to track the face. 
// In this code i only use this to check if there is a face. I do this by checking if the code posts false instead of an array for positions on the face. 
function setupFaceTracker() {
  cameraTracker = new clm.tracker();
  cameraTracker.init(pModel);
  cameraTracker.start(videoCapture.elt);
}

// This function checks if a key is pressed and draws that key on screen at a random location with a size between 30 to 80
// The key will only be drawn if the allowDrawing is true, it also resets the timeSinceLastKeyPress
function keyPressed() {
  if (allowDrawing) {
    textSize(random(30, 80));
    text(key, random(0, windowWidth), random(0, windowHeight));
    moneyMade++;
  }
  timeSinceLastKeyPress = 0;
}

// This function runs everytime the mouse is clicked and makes a big ellipse
// This function only works if allowDrawing is true. It also resets 'timeSinceLastMouseClicked'
function mouseClicked() {
  if (allowDrawing) {
    fill(255);
    ellipse(mouseX, mouseY, 50, 50);
    moneyMade += 5;
  }
  timeSinceLastMouseClicked = 0;
}

// This function tracks the mouse and draws an ellipse at its location. 
// This only works if allowDrawing is true. It also resets 'timeSinceMouseMoved'
function mouseMoved() {
  if (allowDrawing) {
    fill(255);
    ellipse(mouseX, mouseY, 10, 10);
    moneyMade++;
  }
  timeSinceMouseMoved = 0;
}

// When the webcam doesn't track the face, return to this function
function notLookingAtScreen() {
  allowDrawing = false;
  isBackgroundBlack = false;

  if (showFirstMessage) {
    background(255);
    textSize(windowWidth / 20);
    fill(0);
    textAlign(CENTER);
    text('WHERE ARE YOU GOING?', windowWidth / 2, windowHeight / 2);
  } else {
    background(0);
    textSize(windowWidth / 20);
    fill(255);
    textAlign(CENTER);
    text('LOOK AT US!', windowWidth / 2, windowHeight / 2);
  }
}

function toggleMessage() {
  showFirstMessage = !showFirstMessage;
}

// When the webcam tracks the face, go to this function
function lookingAtScreen() {
  // Here I change the background once to black so it doesn't override everything else. 
  // I have chosen to include 'timeSinceLastKeyPress', 'timeSinceLastMouseClicked', 'timeSinceLastMouseMoved' to make sure it resets every time you look at the screen. 
  // I have also included 'allowDrawing' as it also only needs to run once. 
  if (!isBackgroundBlack) {
    resetTimeVariables();
    background(0);
    allowDrawing = true;
    isBackgroundBlack = true;
  }

  // Draw the text in the middle and the counter for the moneyMade
  noStroke();
  textSize(windowWidth / 20);
  fill(255);
  textAlign(CENTER);
  text('PLEASE LET US HARVEST YOU', windowWidth / 2, windowHeight / 2);

  // When the money counter is going up, it needs to be drawn over so a new text can appear.
  fill(0);
  rectMode(CENTER);
  rect(windowWidth / 2, windowHeight / 1.85, windowWidth / 2.3, windowWidth / 30);

  fill(255);
  textSize(windowWidth / 50);
  text(`MONEY WE MADE FROM YOU: ${moneyMade} $`, windowWidth / 2, windowHeight / 1.80);

  // This function tracks time since an action and if a message should be posted
  checkTimeSinceLastActionAndPostMessage();

  // every 20 frames it give money 
  if(frameCount % 20 == 0){
    moneyMade += floor(1)}
}

// Reset time-related variables
function resetTimeVariables() {
  timeSinceLastKeyPress = 0;
  timeSinceLastMouseClicked = 0;
  timeSinceMouseMoved = 0;
}

// This function checks for any action like moving the mouse, typing on the keyboard, and clicking the mouse. If the user hasn't done an action in a while, it will post a message.
function checkTimeSinceLastActionAndPostMessage() {
  // This is a simple way to add to the variable each frame. 
  timeSinceLastKeyPress++;
  timeSinceLastMouseClicked++;
  timeSinceMouseMoved++;

  // If there has gone more than 2 seconds, the message will appear at a random location with a random size between 10 to 50
  if (timeSinceLastKeyPress == 120) {
    textSize(random(10, 50));
    text('PLEASE TYPE SOMETHING', random(0, windowWidth), random(0, windowHeight));
  }

  // If there has gone more than 3 seconds, the message will appear at a random location with a random size between 10 to 50
  if (timeSinceMouseMoved == 180) {
    textSize(random(10, 50));
    text('PLEASE MOVE THE MOUSE', random(0, windowWidth), random(0, windowHeight));
  }

  // If there has gone more than 4 seconds, the message will appear at a random location with a random size between 10 to 50
  if (timeSinceLastMouseClicked == 240) {
    textSize(random(10, 50));
    text('PLEASE CLICK SOMEWHERE', random(0, windowWidth), random(0, windowHeight));
  }
}

// This function switches between 'notLookingAtScreen' that shows when the user's face is not tracked and 'lookingAtScreen' when the user's face is tracked. 
// The code uses the fact that the facetracker will post 'false' when a face is not tracked. It switches between each when it does.
// This function is also a failsafe. The program that tracks the face isn't perfect and runs into errors where it will post false.
// To make the code run more smoothly, I have made the code so it needs at least 30 false outputs before it switches between screens.
  function draw() {
    // When the face is tracked, the facetracker will post an array called 'positions'. If it can't find a face, this array will return false. 
    let positions = cameraTracker.getCurrentPosition();
    if (!positions) {
      consecutiveFalseCount++;
      if (consecutiveFalseCount >= 20) {
        notLookingAtScreen();
      }
    } else {
      consecutiveFalseCount = 0;
      lookingAtScreen();
    }
  }