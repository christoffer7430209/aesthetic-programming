# PLEASE LET US HARVEST YOU

![Screenshot](screenshot.PNG  "Haversting You")
<br>

Please run the code [here](https://christoffer7430209.gitlab.io/aesthetic-programming/Minix04)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

Please view the full repository [here](https://gitlab.com/christoffer7430209/aesthetic-programming/-/tree/main/Minix04)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

####  Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.

"PLEASE LET US HARVEST YOU" unveils an unsettling and controversial program meticulously tracking users' every move, encompassing facial expressions, mouse clicks, and keystrokes. It serves as a stark critique, shedding light on the uncomfortable reality of how major corporations exploit and monetize personal information for profit. Through its critical lens, the program highlights how seemingly insignificant actions, like mouse movements and key presses, are leveraged to craft algorithms tailored to individual users, maximizing financial gain. Drawing parallels to agricultural practices, it underscores the likening of data harvesting to the cultivation of crops, with companies reaping data from individuals for lucrative ends. With persuasive rhetoric and appeals, it raises provocative questions: Is selling your personality a requirement to use the internet?

#### Describe your program and what you have used and learnt.

My program is a data tracking machine that tracks everything from your face, mouse movement, mouse clicks and key presses. Everything that is tracked is turned into feedback on screen and a profit counter goes up. It is a way for the user to see and understand everything they do are being tracked even though they dont see it. 

To do this i have learned and uses new syntaxes. To track the face i have used `clm.tracker()`which tracks by using `videoCapture()`form the webcam. 
To track the mouse i used `mouseClicked()`and `mouseMoved()`and the keyboard `keyPressed()`
Furthermore i have used `%`(remainder operator) to create a flashing image every second frame. 

  

### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->

[Reference for "interactivity" from P5js.org](https://p5js.org/learn/interactivity.html) <br>

[Reference for how to use 'remainder' from learn.newmedia.dog](https://learn.newmedia.dog/tutorials/p5-js/remainder/)
