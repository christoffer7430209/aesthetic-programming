# Heartbeat Throbber


![Throbber](heart.gif  "Throbber")
<br>

Please run the code [here](https://christoffer7430209.gitlab.io/aesthetic-programming/Minix03)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

Please view the full repository [here](https://gitlab.com/christoffer7430209/aesthetic-programming/-/tree/main/Minix03)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

####  Describe your throbber design, both conceptually and technically. – What do you want to explore and/or express? – What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?

My Throbber design is based upon the heartbeat, and is suppose to resemble the beat of the heart. The way the rectangles go out and quickly turn around is both calming and mesmerising. I have chosen to make them rotate while going in and out to make this camera lens or sawblade form which is interesting to look at and makes the animation more alive.

I have used a ‘for-loop’ to make the rectangles. The machine will go through the ‘for-loop’ loop 8 to create the 8 rectangles but with a different angle each time. The output is 8 rectangles with a difference of 45 degrees each time which creates a circle. I have done this so I don’t need to write as much code and I save a lot of lines. It seems lazy but smart at the same time.

#### How is time being constructed in computation (refer to both the reading materials and your coding)?

Wolfgang Ernst talk about ‘micro-temporality’ and how the computer could possibly see time through the microscopic events that it processes. When looking at it this way, then time is the same for computers as it is for human but just at two different speeds. For a human to get an understanding of this time we can use a throbber. Something to indicate and communicate that the machine is going through enormous amount of task while we wait for a program to load. We expect a computer to be super-fast, but when it’s loading its not because its slow. Its because its doing something complicated that takes time. The throbber indicates this to the user in a way a person can understand it with showing what’s its doing. Because it would be impossible to perceive for at human.

#### Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides?

As I wrote before. The throbber hides all the task the computer, this could be everything from loading a page by connect to the source through the internet, to rendering a model in blender through an algorithm.

#### How might we characterize this icon differently?

The loading bar is a great example and is used everywhere. It also indicates that the computer is doing something, while we wait. But with a loading bar there is an end, and most of the time a time estimate.
<br>

  

### **References**

<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->

[Reference for "for()" function form P5js.org](https://p5js.org/reference/#/p5/for) <br>
[Reference for how to use 'push()' and 'pop()' by The Coding Train](https://www.youtube.com/watch?v=o9sgjuh-CBM)