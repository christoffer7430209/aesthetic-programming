function setup() {
    createCanvas(windowWidth, windowHeight);
    // canvas windowheight and width and i put the geometry in the center, its just nice
    angleMode(DEGREES);
    // radians suck lmao
}

let angle = 0;
let x = 10;
let speed = 1
// the varibables for the angle, which the rectangles will rotate around.
// x is for the x position of the rectangles and the line they follow.
// speed is for the speed how fast he rectangles should move in their lins

function draw() {
    noStroke();
    translate(windowWidth / 2, windowHeight / 2);
    // no stroke lookes better, and translate let me put the geometry in the middle
    background(0, 20);
    // by updating the alpha in the background, i can make a trail behind the rectangles. 
    // also it removes the old rectangles so its not just a stright line
    fill(255, 0, 0);
    // a nice red :)

    if (x > 70 || x < -5) {
        speed = -speed;
    } 
    x += speed;
    // to make my rectangles move back and fourth i use the variable speed.
    // the speed updates the rectangles x-coorinate and therefore where it is drawn. 
    // when the x-coordinate reaches 70. the speed is set to negative and the rectangles moves backwards
    // it does this until it reaches x = 0. where the speed is positive again.
    
    for (let i = 0; i < 360; i += 45) {
        // i use a for loop to draw 8 rectangles with a different angle. Here my angle difference is 45. Hence why it stop at 360
        // by doing this i skip lots of line of code

        push();
        // i use push so the rectangles all can have different angles without intereacting with eachother
        rotate(i + angle);
        // when i create the rectangles i want to use the variable giving form the for loop (i). to rotate them
        // i also add 'angle' so the rectangles modes slowly around in a cicle. and make a nice visual. 
        rect(x, 0, 20);
        // the rectangles x-coordinate is a variable so i can move it around with my earlier code. The rest is just a basic rectangle.
        pop();
        // fianlly i pop so the new rectangles can be made without interfference
    }

    angle += 2;
    // this makes the cirlces go around in a circle and creates the nice visual. 

}