// SHOULD BE MOVED TO ANOTHER DOCUMENT
// the upgrade class for all upgrades, includes the most important variables for each upgrade
class Upgrade {
  constructor(upgradeName, startCost, pointProduction, milestone, co2Production, imageName) {
    this.upgradeName = upgradeName;
    this.startCost = startCost;
    this.upgradeCost = startCost;
    this.pointProduction = pointProduction;
    this.milestone = milestone;
    this.co2Production = co2Production
    this.imageName = imageName

    this.upgradeAmount = 0;
    this.visible = false;
    this.contractText =
      `ENVIROMENTAL IMPACT!
    
      Purchasing a ${this.upgradeName} results in ${this.co2Production} tons of CO2 emissions per year. 
      Are you sure you want to purchase a ${this.upgradeName}?` //${} code in string

    // Automatically assign positions based on index in upgrades array
    this.xPosition = 120;
    this.yPosition = 105 + (58 * (upgrades.length + 1));

    buttons.push(this);
    upgrades.push(this);

    // loads data from local storage if available
    this.loadFromLocalStorage();
  }

  checkIfMilestoneReached() {
    if (points >= this.milestone) {
      this.visible = true;
    }
  }

  // this makes sure that only one button is created for each upgrade, as if more were created it would lag the game
  // this also creates a button for each upgrade, places it at the position and gives it a function
  // furthermore it also hides and shows the button when needed
  upgradeButtonGraphics() {
    if (!this.button) {
      this.button = createButton(`buy<br>${this.upgradeName}`);
      this.button.position(this.xPosition+60, this.yPosition);
      this.button.mousePressed(() => this.upgradeClicked());
      this.button.class('styled-button');
      // "Remember these differences when you are working with functions. Sometimes the behavior of regular functions is what you want, if not, use arrow functions." - w3school.com
      this.button.hide();
    }
   
    if (this.visible) {
      this.button.show();
    } else {
      this.button.hide();
    }
  }

  // displays all info for each upgrade as text
  upgradeInfoDisplay() {
    if (this.visible) {
      imageMode(CENTER)
      image(this.imageName, this.xPosition - 30, this.yPosition + 12, 55, 55)
      push()
      if (points >= this.upgradeCost) {
        fill(16, 130, 27)
      } else {
        fill(70)
      }
      text(`${this.upgradeAmount} x`, this.xPosition + 220, this.yPosition, 200, 100);
      text(`${this.pointProduction * this.upgradeAmount} $ps`, this.xPosition + 220, this.yPosition + 25, 200, 100);
      text(`${this.upgradeCost} $`, this.xPosition + 80, this.yPosition, 200, 200)
      text(`${this.pointProduction} $ps`, this.xPosition + 80, this.yPosition + 25, 200, 100);
      pop()
    }
  }

  // checks if the player has enough points for each upgrade
  // checks if first time clicked 
  upgradeClicked() {
    if (points >= this.upgradeCost && !contractVisible) {
      if (this.upgradeAmount == 0) {
        this.showContract();
      } else {
        points -= this.upgradeCost
        this.upgradeAmount++
        this.upgradeCost = floor(this.startCost * (1.15 ** this.upgradeAmount))
        // local storage
        this.saveToLocalStorage()
      }
    }
  }

  // show the contract
  showContract() {
    contractVisible = true;
    contractText = this.contractText;
    showContractButtons();
    yesButton.mousePressed(() => this.acceptContract());
    noButton.mousePressed(() => this.declineContract());
  }

  acceptContract() {
    points -= this.upgradeCost
    this.upgradeAmount++
    this.upgradeCost = floor(this.startCost * (1.15 ** this.upgradeAmount))
    hideContractButtons()
    contractVisible = false
    this.saveToLocalStorage()
  }

  declineContract() {
    hideContractButtons()
    contractVisible = false
  }


  // adds points for how many of an upgrade the player has
  upgradePointPayout() {
    points += (this.pointProduction * this.upgradeAmount) / 60;
    displayPoints = floor(points);
  }

  saveToLocalStorage() {
    storeItem(`${this.upgradeName}Amount`, this.upgradeAmount)
    storeItem(`${this.upgradeName}Cost`, this.upgradeCost)
  }

  loadFromLocalStorage() {
    let checkIfNullAmount = getItem(`${this.upgradeName}Amount`)
    let checkIfNullCost = getItem(`${this.upgradeName}Cost`)
    if(checkIfNullAmount !== null && checkIfNullCost !== null){
      this.upgradeAmount = getItem(`${this.upgradeName}Amount`)
      this.upgradeCost = getItem(`${this.upgradeName}Cost`)
    }
  }

  gameOverHideDisplay() {
    if (endGame == true) {this.button.hide()}
  }

}


