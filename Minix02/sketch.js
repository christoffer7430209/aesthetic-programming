// The goal of MINIX2 is to create two emojis with geometry 
// I want to create an emoji that is randomized. Therefor i will have 6 eyes and 6 mouths. Which make it possible to create 36 different emojis
let font
function preload() {
  font = loadFont("PB.ttf")
}

let eyes
let mouth
let eyeChoice = ["fear", "happy", "angry", "sad", "surprise", "disgust"]
let mouthChoice = ["fear", "happy", "angry", "sad", "surprise", "disgust"]

// the 6 different types of eys and mouth in a array, so i can pick afterwards.

// values for the eyes and mouth
function setup() {
  createCanvas(600, 600)
  background(230)
  strokeWeight(10)

  //text
  textFont(font)
  textAlign(CENTER)
  textSize(40)
  fill(20)
  text("CLICK TO CHANGE HOW YOU FEEL", 300, 120)
  randomizeFace()
}

function randomizeFace() {
  background(230)
  eyes = random(eyeChoice)
  mouth = random(mouthChoice)
  drawFace(); 
};
// This function runs everytime "randomize" button is pressed

// when you click it runs the ranomize face function
function mouseClicked() {
  randomizeFace()
}

function drawFace() {
  text("CLICK TO CHANGE HOW YOU FEEL", 300, 120)
  // this function makes it possible to have 4 differnt eyes and mouths
  // i have used ellipses and arcs to create simple eyes and mouths which when combined becomes a face
  stroke(floor(random(0, 255)), floor(random(0, 255)), floor(random(0, 255)))
  fill(230)
  // these if statements are for eyes
  if (eyes == "fear") {
    arc(400, 250, 100, 100, -QUARTER_PI, PI+QUARTER_PI)
    line(367, 213, 433, 213)
    arc(200, 250, 100, 100, -QUARTER_PI, PI+QUARTER_PI)
    line(167, 213, 233, 213)
  }
  if (eyes == "happy") {
    arc(400, 275, 100, 100, PI, 0)
    arc(200, 275, 100, 100, PI, 0)
  }
  if (eyes == "angry") {
    arc(400, 250, 100, 100, PI + HALF_PI + QUARTER_PI, QUARTER_PI + HALF_PI)
    line(433, 216, 363, 284,)
    arc(200, 250, 100, 100, QUARTER_PI, PI + QUARTER_PI) 
    line(167, 216, 237, 284)
  }
  if (eyes == "sad") {
    arc(400, 250, 100, 100, 0, PI)
    arc(200, 250, 100, 100, 0, PI)
  }
  if (eyes == "surprise") {
    ellipse(200, 250, 100, 100)
    ellipse(400, 250, 100, 100)
  }
  if (eyes == "disgust") {
    arc(400, 250, 100, 100, PI-QUARTER_PI, QUARTER_PI)
    line(167, 287, 233, 287)
    arc(200, 250, 100, 100, PI-QUARTER_PI, QUARTER_PI)
    line(367, 287, 433, 287)
  }
  // these if statements is for mouthes 
  if (mouth == "fear") {
    ellipse(300, 440, 160, 80)
  }
  if (mouth == "happy") {
    arc(300, 400, 200, 100, 0, PI)
  }
  if (mouth == "angry") {
    line(225, 440, 375, 400)
  }
  if (mouth == "sad") {
    arc(300, 440, 200, 100, PI, 0)
  }
  if (mouth == "surprise") {
    ellipse(300, 440, 120, 80)
  }
  if (mouth == "disgust") {
    line(225, 400, 375, 400);
  }
  
}

function draw() {}
