let direction;
let lastDirection;
let distanceMoved 
let distance;
let colorOfSphere;
let x = 0
let y = 0
let z = 0

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  background(0)
  colorOfSphere = [floor(random(0, 255)), floor(random(0, 255)), floor(random(0, 255))]
  direction = random(["right", "left", "up", "down", "out", "in"])
  choseDirection()
  choseDistance()
}


function choseDirection(){
  if(direction == "right" || direction == "left"){
    direction = random(["up", "down", "out", "in"])}
  else if (direction == "up" || direction == "down"){
    direction = random(["right", "left", "out", "in"])}
  else if (direction == "in" || direction == "out"){
    direction = random(["right", "left", "up", "down"])}
// this function picks a direction in the 3d space
// the driection chosen cannot be in the same direction or the oppesite. so its looks cleaner.
}

function choseDistance(){
  distanceMoved = 0
  distance = floor((random(10, 50)))
// this function resets the distanceMoved and picks a new distance between 10 and 50
}

function goInDirection(){
  if (direction === "up"){y -= 5}
  if (direction === "down"){y += 5}
  if (direction === "right"){x += 5}
  if (direction === "left"){x -= 5}
  if (direction === "out"){z += 5}
  if (direction === "in"){z -= 5}
// this fucntion checks for which direction has been chosen and makes the sphere move by changing the xyz on the sphere
  if (++distanceMoved === distance) {
  choseDirection();
  choseDistance();
  }
// the sphere needs to change direction so when the chosen distance is the same as distance moved. it chooses a new direction and distance. 
}

function outOfBounds(){
  if(x >=  400  || y >= 400 || z >= 400 || x <= -400 || y <= -400 || z <= -400 ){
    colorOfSphere = [floor(random(0, 255)), floor(random(0, 255)), floor(random(0, 255))]
    x = 0
    y = 0
    z = 0
  }
// this function resets the sphere when it goes to far away form the start. It sets the xyz to 0 and pick a new color. 
}


function cameraAndLight(){
  pointLight(250, 250, 250, 500, 500, 500);
  ambientLight(200);
// gives the sphere an articfical light. so its prettier and easier to see.
  camera(800, -800, 800)
// changes the camera angle 
}


function drawSphere(){
  noStroke()
  fill(colorOfSphere)
  translate(x, y, z)
  sphere(20)
// creates, position and colors the sphere
}

function draw() {
  outOfBounds();
  goInDirection()
  cameraAndLight()
  drawSphere()
}

// https://p5js.org/reference/#/p5/pointLight
// https://p5js.org/learn/getting-started-in-webgl-coords-and-transform.html
// https://p5js.org/reference/#/p5/ambientLight
// https://p5js.org/reference/#/p5/camera

 