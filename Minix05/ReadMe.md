
# PIPES

  

![GIF](pipes.gif  "PIPES")

<br>

  

Please run the code [here](https://christoffer7430209.gitlab.io/aesthetic-programming/Minix05)

  

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

  

Please view the full repository [here](https://gitlab.com/christoffer7430209/aesthetic-programming/-/tree/main/Minix05)

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

#### What are the rules in your generative program? Describe how your program performs over time? 
I have set up two rules for my program. I want it to be heavily randomized and it should create an artwork forever, so it is always changing. It should be nearly impossible to have two of the same artworks while running the program. 

#### How do the rules produce emergent behavior? What role do rules and processes have in your work?
I have chosen to have three different randomizers in my code. One for which direction the sphere should go which there is 6: ‘left’, ‘right’, ‘up’, ‘down’, ‘in’, ‘out’. One for the distance the spheres should go 10 to 50. And one for a random color everytime the sphere is reset These three randomizers make it high unlikely that one sphere will move and look the same. Furthermore, the artwork is being slowly being made and the longer you let it run the more pipes are being made. Because of this the artwork will always change and never stay the same.

#### Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? 
The randomizers for the direction is not truly random. I have chosen to not let the program keep the same direction and the opposite direction to not let the sphere move to far away or into itself. Even though its moves randomly I have chosen to take control over the randomness to not let it run rampant and look weird.


### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
[Reference for "pointlight" from P5js.org](https://p5js.org/reference/#/p5/pointLight) <br>
[Reference for "ambientLight" from P5js.org](https://p5js.org/reference/#/p5/ambientLight) <br>
[Reference for "camera" from P5js.org](https://p5js.org/reference/#/p5/camera) <br>
[Reference for "Getting started in WEBGL" from P5js.org](https://p5js.org/learn/getting-started-in-webgl-coords-and-transform.html)